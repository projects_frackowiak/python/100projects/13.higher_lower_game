import game_data
import logo
import random

def first():
    first_dic = random.choice(game_data.data)
    return first_dic
def secound(first_dic):
    notTheSame = True
    secound_dic = random.choice(game_data.data)
    while notTheSame:
        if secound_dic == first_dic:
            secound_dic = random.choice(game_data.data)
        else:
            notTheSame = False
    return secound_dic
def rigth():
        global score
        score += 1
        return "You're right!"
def compare(choise,tab):
    if choise == 'A'and tab[0]['follower_count'] > tab[1]['follower_count']:
        print(rigth())
        return 'A'
    elif choise == 'B'and tab[0]['follower_count'] < tab[1]['follower_count']:
        print(rigth())
        return 'B'
    else:
        global score
        print(f"You're wrong! Your score is {score}")
        score = 0
        return 'Lose'
def show(tab):
    first_dic = tab[0]
    secound_dic = tab[1]
    print(logo.logo)
    print(f"Your current score is {score}")
    print(f"Compare A: {first_dic['name']}, a {first_dic['description']}, from {first_dic['country']}")
    print(logo.vs)
    print(f"Compare B: {secound_dic['name']}, a {secound_dic['description']}, from {secound_dic['country']}")
    return
def play_game(tab):
    show(tab)
    choice = compare(input("Who has more followers? Type 'A' or 'B': " ),tab)
    if score != 0:
        if choice == 'A':
            del tab[1]
        elif choice == 'B':
            del tab[0]
        tab.append(secound(tab[0]))
        play_game(tab)
    else:
        one_more = input("Wanna play one more? Type 'y' or 'n': ")
        if one_more == 'y':
            del tab[0]
            del tab[0]
            tab.append(first())
            tab.append(secound(tab[0]))
            play_game(tab)
        else:
            print("Thank's for game")

score = 0
tab = []
tab.append(first())
tab.append(secound(tab[0]))
play_game(tab)